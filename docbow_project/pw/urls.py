from django.conf import settings
from django.urls import include, path

if 'mellon' in settings.INSTALLED_APPS:
    urlpatterns = [
        path('accounts/mellon/', include('mellon.urls')),
    ]
else:
    urlpatterns = []
