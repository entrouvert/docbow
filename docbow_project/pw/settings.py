import os

from docbow_project.settings import *  # noqa: F403

DEFAULT_FROM_EMAIL = 'gestionnaire@courrier.parlement-wallon.be'
CONTACT_SUBJECT_PREFIX = 'Contact depuis courrier.parlement-wallon.be: '
AUTOMATIC_FORWARDING_USER = 'Greffe'
SERVER_EMAIL = DEFAULT_FROM_EMAIL

DOCBOW_PERSONAL_EMAIL = False
DOCBOW_MOBILE_PHONE = False
DOCBOW_GROUP_LISTING = False
DOCBOW_DELEGATE_TO_EXISTING_USER = False
DOCBOW_DEFAULT_ACCEPT_NOTIFICATIONS_FOR_GUEST = False

if 'USE_SAML' in os.environ:
    INSTALLED_APPS += ('mellon',)  # noqa: F405
    LOGIN_URL = 'mellon_login'
    LOGOUT_URL = 'mellon_logout'
    AUTHENTICATION_BACKENDS = ('mellon.backends.SAMLBackend',)
    MELLON_IDENTITY_PROVIDERS = {
        'METADATA': '/etc/docbow/idp-metadata.xml',
        'GROUP_ATTRIBUTE': 'role',
    }
    MELLON_ATTRIBUTE_MAPPING = {
        'first_name': '{attributes[sn][0]}',
        'last_name': '{attributes[gn][0]}',
        'email': '{attributes[mail][0]}',
    }
    MELLON_SUPERUSER_MAPPING = {
        'role': ['Superutilisateur'],
    }
    MELLON_USERNAME_TEMPLATE = '{attributes[name_id_content]}'

if 'AUTHENTIC_DATABASE_ENGINE' in os.environ:
    DATABASES['authentic'] = {  # noqa: F405
        'ENGINE': os.environ.get('AUTHENTIC_DATABASE_ENGINE'),
        'NAME': os.environ.get('AUTHENTIC_DATABASE_NAME'),
        'USER': os.environ.get('AUTHENTIC_DATABASE_USER'),
    }

JOURNAL_DB_FOR_ERROR_ALIAS = 'journal'
DATABASES['journal'] = DATABASES['default'].copy()  # noqa: F405
DATABASES['journal']['TEST_MIRROR'] = 'default'  # noqa: F405
BASE_URL = 'https://courrier.parlement-wallon.be'
EMAIL_SUBJECT_PREFIX = ''

if PLATFORM == 'test':  # noqa: F405
    BASE_URL = 'https://pes-pw.dev.entrouvert.org'
    DEFAULT_FROM_EMAIL = 'gestionnaire@pes-pw.dev.entrouvert.org'
    CONTACT_SUBJECT_PREFIX = 'Contact depuis pes-pw.dev.entrouvert.org: '

HELP_DIR = '/usr/share/doc/docbow/build-pw'

try:
    from local_settings import *  # noqa: F403
except ImportError:
    pass
