Extension PFWB pour Docbow
==========================

Cette extension fournit de nombreuses adaptations au contexte du PFWB, comme
l'export de document vers la GED en utilisant un répertoire ou l'importation
de document envoyé par Tabellio expédition via SMTP.

Configuration
=============

Export GED
----------

Vous devez ajouter les lignes suivantes à votre fichier local_settings.py::

    from docbow_project.settings.dev import INSTALLED_APPS
    INSTALLED_APPS += ('docbow_project.pfwb',)

Ensuite vous devez définir dans ce même local_settings.py le répertoire où les
fichiers attachés seront déposés, par exemple::

    DOCBOW_PFWB_GED_DIRECTORY = '/var/lib/plone/docbow_import_directory/'

Réception SMTP des documents issues de Tabellio expédition
----------------------------------------------------------

PFWB_SENDMAIL_DEFAULT_TYPE_ID: identifiant d'objet FileType par défaut pour
les mails dont le type ne peut être identifié
PFWB_SENDMAIL_DEFAULT_TYPE_NAME: nom par défaut à donner à l'object FileType
par défaut si celui-ci n'existe pas encore
PFWB_SENDMAIL_TABELLIO_EXPEDITION_EMAIL: email à reconnaitre dans les mails
issus de Tabellio expédition
PFWB_SENDMAIL_TABELLIO_EXPEDITION_USER_ID: identifiant de l'utilisateur
désigné comme expéditeur par défaut des documents issus de Tabellio expédition
PFWB_SENDMAIL_ATTACHED_FILE_EMAIL: email à reconnaitre dans les mails reçus
via l'import SMTP générique (avec fichier attaché)
PFWB_SENDMAIL_ATTACHED_FILE_USER_ID: identifiant de l'utilisateur désigné
comme expéditeur par défaut des documents reçus via l'import SMTP générique

Synchronisation avec Tabellio
-----------------------------

 - PARLEMENTAIRES_MAILING_ID: identifiant de la liste des destinataires pour les députés
 - MINISTRES_MAILING_ID: identifiant de la liste des destinataires pour les ministres
 - TABELLIO_DBHOST: nom d'hôte de la base de donnée Tabellio
 - TABELLIO_DBPORT: port TCP de la base de donnée Tabellio
 - TABELLIO_DBNAME: nom la base de donnée Tabellio
 - TABELLIO_DBUSER: nom d'utilisateur pour accéder à la base de donnée Tabellio
 - TABELLIO_DBPASSWORD: mot de passe pour accéder à la base de donnée Tabellio

Nommage des fichiers exportés
=============================

Pour tout fichier attaché nommé par exemple my_document.pdf l'export GED
crééra les fichiers suivants:


    2013-01-12T23:24:24.34333-my_document.pdf
    2013-01-12T23:24:24.34333-my_document.pdf.json

Ces noms de fichier sont composés des données disponibles au moment du dépôt
du document et du nom du fichier original. Le fichier JSON est un dictionnaire
contenant les clés suivantes:

Clé               Description
================= ======================
reception_date    Date et heure de dépôt au format ISO8601
description       La description du document
sender            Prénom, nom et identifiant entre parenthèse de l'expéditeur
plone_portal_type Le type de fichier GED configuré pour ce type de document
title             Le type de document de la plateforme d'échange sécurisée

Archivage
=========

Une command d'archivage est disponible qui permet d'archiver dans un
répertoire tous les documents et les logs antérieurs à une certaine date. Le
seuil d'antériorité est donné via un nombre de jour, pour les documents ayant
plus d'un an on donnera comme nombre de jours 366.

Pour archive dans /opt/archive les documents d'il y a plus d'un an::

  /etc/init.d/docbow manage archive2 /opt/archive 366

Un répertoire, nommé /opt/archive/2013-01-01T16:34:34.343434/doc/ par exemple,
sera créé contenant un sous-répertoire par document. Chaque sous répertoire
est nommé d'après l'identifiant du document en base il contient au moins 3
fichiers:

Nom                     Description
======================= ==================
document.json           la sérialisation JSON du modèle du document faite par
                        Django
attached_file_<id>.json la sérialisation JSON du modèle d'un fichier attaché
<filename>.<ext>        le contenu d'un fichier attaché, le même nom de
                        fichier apparait dans le document JSON

Si plusieurs fichiers sont attachés à un même document, il y aura un fichier
JSON et un fichier de contenu pour chacun d'entre eux.

Les lignes du journal seront sauvegardées dans le fichier
/opt/archive/2013-01-01T16:34:34.343434/journal.txt
