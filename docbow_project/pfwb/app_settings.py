import sys


class AppSettings:
    '''Thanks django-allauth'''

    __DEFAULTS = dict(
        # directory where ged files are stored
        PFWB_GED_DIRECTORY=None,
        # default type id for documents received by SMTP when given type does
        # not exist
        PFWB_SENDMAIL_DEFAULT_TYPE_ID=None,
        # default type name if default type id does not exist
        PFWB_SENDMAIL_DEFAULT_TYPE_NAME='Divers',
        # sender email for document received from tabellio expedition by SMTP
        PFWB_SENDMAIL_TABELLIO_EXPEDITION_EMAIL='commande.documents@pfwb.be',
        # user id of senders for document received from tabellio expedition by SMTP
        PFWB_SENDMAIL_TABELLIO_EXPEDITION_USER_ID=None,
        # sender email for document received by SMTP (generic code)
        PFWB_SENDMAIL_ATTACHED_FILE_EMAIL=None,
        # user id of senders for document received by SMTP (generic code)
        PFWB_SENDMAIL_ATTACHED_FILE_USER_ID=None,
    )

    def __init__(self, prefix):
        self.prefix = prefix

    @property
    def settings(self):
        from django.conf import settings

        return settings

    def __getattr__(self, key):
        if key in self.__DEFAULTS:
            return getattr(self.settings, self.prefix + key, self.__DEFAULTS[key])
        else:
            from django.core.exceptions import ImproperlyConfigured

            try:
                return getattr(self.settings, self.prefix + key)
            except AttributeError:
                raise ImproperlyConfigured('settings %s is missing' % self.prefix + key)


app_settings = AppSettings('DOCBOW_')
app_settings.__name__ = __name__
app_settings.__file__ = __file__

sys.modules[__name__] = app_settings
