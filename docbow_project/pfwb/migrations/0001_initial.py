from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('docbow', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='PloneFileType',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('plone_portal_type', models.CharField(max_length=64)),
                (
                    'filetype',
                    models.OneToOneField(
                        verbose_name='Document type', to='docbow.FileType', on_delete=models.CASCADE
                    ),
                ),
            ],
            options={
                'db_table': 'plone_plonefiletype',
                'verbose_name': 'Plone file type',
                'verbose_name_plural': 'Plone file types',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TabellioDocType',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('tabellio_doc_type', models.CharField(max_length=64, verbose_name='Tabellio doc type')),
                (
                    'filetype',
                    models.OneToOneField(
                        verbose_name='Document type', to='docbow.FileType', on_delete=models.CASCADE
                    ),
                ),
            ],
            options={
                'verbose_name': 'Tabellio doc type mapping',
                'verbose_name_plural': 'Tabellio doc type mappings',
            },
            bases=(models.Model,),
        ),
    ]
