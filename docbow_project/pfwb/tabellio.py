""" Partially generated with sqlacodegen.
https://pypi.org/project/sqlacodegen/
"""


from sqlalchemy import ARRAY, TEXT, Boolean, Column, Date, DateTime, ForeignKey, Index, Integer, Text
from sqlalchemy.dialects.postgresql import TSVECTOR
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker

Base = declarative_base()
DBSession = sessionmaker()


class TAdresse(Base):
    __tablename__ = 't_adresse'

    id = Column(Text, primary_key=True)
    titre = Column(Text)
    batbur = Column(Text)
    rueno = Column(Text)
    zip = Column(Text)
    localite = Column(Text)
    tel1 = Column(Text)
    tel2 = Column(Text)
    fax = Column(Text)
    email = Column(Text)
    courrier = Column(Boolean, nullable=False)
    courriel = Column(Boolean, nullable=False)


class TPer(Base):
    __tablename__ = 't_pers'
    __table_args__ = (Index('i_t_pers_persobjet', 'nom', 'prenom', 'initiales', unique=True),)

    id = Column(Text, primary_key=True)
    st = Column(Text, nullable=False)
    ts = Column(DateTime)
    nom = Column(Text, nullable=False)
    prenom = Column(Text)
    initiales = Column(Text, nullable=False)
    titre = Column(Text)
    sexe = Column(Text)
    datenaiss = Column(Date)
    datedeces = Column(Date)
    addrpriv = Column(ForeignKey('t_adresse.id'))
    addrprof1 = Column(ForeignKey('t_adresse.id'))
    addrprof2 = Column(ForeignKey('t_adresse.id'))

    t_adresse = relationship('TAdresse', primaryjoin='TPer.addrpriv == TAdresse.id')
    t_adresse1 = relationship('TAdresse', primaryjoin='TPer.addrprof1 == TAdresse.id')
    t_adresse2 = relationship('TAdresse', primaryjoin='TPer.addrprof2 == TAdresse.id')


class TTypehistoper(Base):
    __tablename__ = 't_typehistopers'

    id = Column(Text, primary_key=True)
    st = Column(Text, nullable=False)
    ts = Column(DateTime)
    descr = Column(Text, nullable=False)


class TPershistoline(Base):
    __tablename__ = 't_pershistoline'

    id = Column(Text, primary_key=True)
    st = Column(Text, nullable=False)
    ts = Column(DateTime)
    pers = Column(ForeignKey('t_pers.id'), nullable=False)
    debut = Column(Date, nullable=False)
    fin = Column(Date)
    type = Column(ForeignKey('t_typehistopers.id'), nullable=False)
    description_fts = Column(TSVECTOR, index=True)
    description = Column(Text)
    commentaire_fts = Column(TSVECTOR, index=True)
    commentaire = Column(Text)

    t_per = relationship('TPer', backref='tperhistolines')
    t_typehistoper = relationship('TTypehistoper')


class TComppol(Base):
    __tablename__ = 't_comppol'

    id = Column(Text, primary_key=True)
    st = Column(Text, nullable=False)
    ts = Column(DateTime)
    abbr = Column(Text, nullable=False)
    membres = Column(Integer, nullable=False)
    groupe = Column(Boolean, nullable=False)
    nom = Column(Text, nullable=False)
    ordre = Column(Integer)


class TCom(Base):
    __tablename__ = 't_com'

    id = Column(Text, primary_key=True)
    st = Column(Text, nullable=False)
    ts = Column(DateTime)
    nom = Column(Text, nullable=False)
    code = Column(Text, nullable=False)
    baseleg = Column(Text)
    limite = Column(Date)
    comprinc = Column(ForeignKey('t_com.id'))
    burvicepres = Column(ARRAY(TEXT()))
    bursecr = Column(ARRAY(TEXT()))
    titulaires = Column(ARRAY(TEXT()))
    suppleants = Column(ARRAY(TEXT()))
    compets = Column(ARRAY(TEXT()))
    ordre = Column(Integer)

    parent = relationship('TCom', remote_side=[id])


class TTypedoc(Base):
    __tablename__ = 't_typedoc'

    id = Column(Text, primary_key=True)
    st = Column(Text, nullable=False)
    ts = Column(DateTime)
    descr = Column(Text, nullable=False)
    fno = Column(Boolean, nullable=False)
    fnodoc = Column(Boolean, nullable=False)
    listnum = Column(Boolean, nullable=False)
    finet = Column(Boolean)
