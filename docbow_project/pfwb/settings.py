from docbow_project.settings import *  # noqa: F403

DOCBOW_EDIT_EMAIL = True


PORTAL_BASE_URL = 'https://form.portail.pfwb.be'

HELP_DIR = '/usr/share/doc/docbow/build-pfwb'
