from django.db import models
from django.utils.translation import gettext_lazy as _

from ..docbow.models import FileType


class PloneFileType(models.Model):
    '''GED file type to store in exported JSON documents'''

    filetype = models.OneToOneField(FileType, verbose_name=_('Document type'), on_delete=models.CASCADE)
    plone_portal_type = models.CharField(max_length=64)

    class Meta:
        # backward compatibility
        db_table = 'plone_plonefiletype'
        verbose_name = _('Plone file type')
        verbose_name_plural = _('Plone file types')


class TabellioDocType(models.Model):
    '''Mapping from Tabellio document type to Docbow filetype'''

    filetype = models.OneToOneField(FileType, verbose_name=_('Document type'), on_delete=models.CASCADE)
    tabellio_doc_type = models.CharField(max_length=64, verbose_name=_('Tabellio doc type'))

    class Meta:
        verbose_name = _('Tabellio doc type mapping')
        verbose_name_plural = _('Tabellio doc type mappings')
