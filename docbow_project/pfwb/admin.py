from django.contrib import admin

import docbow_project.docbow.admin as docbow_admin
from docbow_project.pfwb import models


class PloneFileTypeInlineAdmin(admin.StackedInline):
    model = models.PloneFileType
    extra = 0


if getattr(docbow_admin.FileTypeAdmin, 'inlines'):
    docbow_admin.FileTypeAdmin.inlines += [PloneFileTypeInlineAdmin]
else:
    docbow_admin.FileTypeAdmin.inlines = [PloneFileTypeInlineAdmin]
