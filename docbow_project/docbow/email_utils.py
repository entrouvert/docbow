import re
from email.header import decode_header, make_header

from django.utils.encoding import force_str

# check spaces between encoded_words (and strip them)
sre = re.compile(r'\?=[ \t]+=\?')
# re pat for MIME encoded_word (without trailing spaces)
mre = re.compile(r'=\?[^?]*?\?[bq]\?[^? \t]*?\?=', re.I)


def decode_mime(m):
    # substitute matching encoded_word with force_text equiv.
    h = decode_header(m.group(0))
    u = force_str(make_header(h))
    return u


def u2u_decode(s):
    # utility function for (final) decoding of mime header
    # note: resulting string is in one line (no \n within)
    # note2: spaces between enc_words are stripped (see RFC2047)
    s = ''.join(s.splitlines())
    s = sre.sub('?==?', s)
    u = mre.sub(decode_mime, s)
    return u
