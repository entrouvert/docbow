$(function () {
  $('#id_not_after, #id_not_before').datepicker({
    changeYear: true,
    changeMonth: true,
    dateFormat: 'd/m/yy',
  });

  var boxtype = $("#id_search_terms").data('boxtype')
  $( "#id_search_terms" ).autocomplete({
    source: '/search-' + boxtype
  });
});
