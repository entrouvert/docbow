function get_checkboxes_with_same_value(elt) {
  var $elt = $(elt);
  var value = $elt.val();
  return $('input[value=' + value + ']').not(elt);
}

function are_all_checked(elt) {
  var $all = get_checkboxes_with_same_value(elt).not('.js-checkall-value');
  for (var i = 0; i < $all.length; i++) {
    if (! $all[i].checked) {
      return false;
    }
  }
  return true;
}

$(function () {
  $('.js-checkall-value').each(function (i, elt) {
    var $all = get_checkboxes_with_same_value(elt);
    function update() {
      if (are_all_checked(elt)) {
        $(elt).prop('checked', true);
      } else {
        $(elt).prop('checked', false);
      }
    }
    update();
    $(elt).change(function (event) {
      if (elt.checked) {
        $all.prop('checked', true);
      } else {
        $all.prop('checked', false);
      }
    })
    $all.change(function (event) {
      update();
    })
  })
})
