function after() {
	django.jQuery('.selector-chosen h2').append('<div style="float: right" id="chosen-count"></div>');

	var old_redisplay = SelectBox.redisplay

	function new_redisplay(id) {
		var chosen_count = django.jQuery('#chosen-count');
		chosen_count.html(SelectBox.cache.id_members_to.length+' personnes');
		old_redisplay(id);
	}
	SelectBox.redisplay = new_redisplay
	new_redisplay('id_members_to');
};
django.jQuery(function () {
	setTimeout(after, 500);
});
