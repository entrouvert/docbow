(function ($, undef) {
  $(document).bind('change', function () {
      window.onbeforeunload = function () {
          return "Vous avez essayé de quitter cette page. Si vous avez effectué des modifications à des champs du formulaire sans le soumettre, ces modifications seront perdues. Voulez-vous vraiment quitter cette page ?";
      }
    })
  $(document).bind('submit', function () {
      window.onbeforeunload = function (){};
  });
})(jQuery)
