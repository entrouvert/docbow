(function ($, undef) {
  var install;
  var tsr = function () {
    var url = window.location.href;
    $('.refresh').each(function (i, elt) {
      var id = elt.id;
      $(elt).load(url + ' #' + id + ' > *');
    });
    install();
  };
  var callback;
  install = function () {
    if (callback) {
      clearTimeout(callback);
    }
    callback = setTimeout(tsr, 30000);
  }
  install();
  $('body').mousemove(install);
  $('body').keyup(install);
})(jQuery)
