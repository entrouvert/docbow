function getParameterByName(url, name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(url);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

$(function() {
  var url = $('#send-file-form').data('init-url');
  if (! url) return;
  var oReq = new XMLHttpRequest();
  oReq.open('GET', url, true);
  oReq.responseType = 'blob';
  oReq.withCredentials = true;
  oReq.onload = function(oEvent) {
    var blob = oReq.response;
    var file = null;
    var content_disposition = oReq.getResponseHeader('content-disposition');
    if (content_disposition) {
      file = new File([blob], content_disposition.split('"')[1]);
    } else {
      var filename = getParameterByName(url, 'filename');
      if (filename) {
        file = new File([blob], filename);
      } else {
        file = new File([blob], 'doc');
      }
    }
    $('#content_1-fileupload').fileupload('add', {files: [file]});
  }
  oReq.onerror = function(oEvent) {
    console.log('error :/', oReq.status, oReq.statusText, oReq.readyState);
  }
  oReq.send();
  return;
});
