from django.contrib.auth import views as auth_views
from django.urls import path, re_path, reverse_lazy

from docbow_project.docbow import auth_views as docbow_auth_views
from docbow_project.docbow import forms, views

urlpatterns = [
    path('login/', docbow_auth_views.login, {'template_name': 'registration/login.html'}, name='auth_login'),
    path(
        'logout/',
        auth_views.LogoutView.as_view(template_name='registration/logout.html'),
        name='auth_logout',
    ),
    path('password/change/', views.password_change, name='auth_password_change'),
    path(
        'password/change/done/',
        auth_views.PasswordChangeDoneView.as_view(),
        name='auth_password_change_done',
    ),
    path(
        'password/reset/',
        auth_views.PasswordResetView.as_view(
            success_url=reverse_lazy('auth_password_reset_done'),
            form_class=forms.PasswordResetFormWithLogging,
        ),
        name='auth_password_reset',
    ),
    re_path(
        r'^password/reset/confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$',
        auth_views.PasswordResetConfirmView.as_view(),
        name='auth_password_reset_confirm',
    ),
    path(
        'password/reset/complete/',
        auth_views.PasswordResetCompleteView.as_view(),
        name='password_reset_complete',
    ),
    path('password/reset/done/', views.password_reset_done, name='auth_password_reset_done'),
]
