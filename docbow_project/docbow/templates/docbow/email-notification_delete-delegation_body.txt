{% autoescape off %}{{user.get_full_name}} vous a retiré la délégation de son compte
{{user.username}} sur la plate-forme sécurisée d'échange de documents{% if settings.DOCBOW_ORGANIZATION %} du 
{{settings.DOCBOW_ORGANIZATION}}{% endif %}.

{% include "docbow/email_signature.txt" %}{% endautoescape %}
