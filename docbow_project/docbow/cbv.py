class FormWithRequestMixin:
    def get_form_kwargs(self, **kwargs):
        kwargs = super().get_form_kwargs(**kwargs)
        kwargs['request'] = self.request
        return kwargs


class FormWithPrefixMixin:
    # deprecated after Django 1.6
    prefix = None

    def get_form_kwargs(self, **kwargs):
        kwargs = super().get_form_kwargs(**kwargs)
        kwargs['prefix'] = self.prefix
        return kwargs


class FormWithPostTarget(FormWithPrefixMixin):
    def get_form_kwargs(self, **kwargs):
        kwargs = super().get_form_kwargs(**kwargs)
        if not self.is_post_target():
            kwargs.pop('data', None)
            kwargs.pop('files', None)
        return kwargs

    def is_post_target(self):
        return self.prefix + '-validate' in self.request.POST
