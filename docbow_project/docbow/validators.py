import re

from django.core.validators import RegexValidator

validate_phone = RegexValidator(r'^\+\d+$')
validate_fr_be_phone = RegexValidator(r'^\+(?:33[67]\d{8}|324\d{8})$')


def phone_normalize(phone_number):
    '''Remove usual separators from phone number.'''
    return re.sub(r'[\.\-\s]', '', phone_number)
