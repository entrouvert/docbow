from django.urls import re_path

import docbow_project.docbow.upload_views

urlpatterns = [
    re_path(
        r'^(?P<transaction_id>[a-zA-Z0-9-]+)/(?P<file_kind>[0-9]+)/$',
        docbow_project.docbow.upload_views.upload,
        name='upload',
    ),
    re_path(
        r'^(?P<transaction_id>[a-zA-Z0-9-]+)/$', docbow_project.docbow.upload_views.upload, name='upload'
    ),
    re_path(
        r'^(?P<transaction_id>[a-zA-Z0-9-]+)/(?P<filename>[^/]+)$',
        docbow_project.docbow.upload_views.upload_file,
        name='uploaded',
    ),
]
