from django.conf import settings
from django.shortcuts import resolve_url


def settings_url_processor(request):
    logout_url = settings.LOGOUT_URL
    logout_url = resolve_url(logout_url)
    return {
        'logout_url': logout_url,
        'portal_base_url': getattr(settings, 'PORTAL_BASE_URL', None),
    }
