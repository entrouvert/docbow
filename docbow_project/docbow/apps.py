import django.apps


class AppConfig(django.apps.AppConfig):
    name = 'docbow_project.docbow'

    def ready(self):
        from . import signals  # noqa: F401
