from django.conf import settings
from django.urls import include, path, re_path

import docbow_project.docbow.views

urlpatterns = [
    path('', docbow_project.docbow.views.homepage, name='homepage'),
    path('profile/', docbow_project.docbow.views.profile, name='profile'),
    # inbox
    path('inbox/', docbow_project.docbow.views.inbox_view, name='inbox'),
    path('inbox/trash/', docbow_project.docbow.views.inbox_trash_view, name='inbox-trash'),
    path(
        'inbox_by_document/<int:document_id>/',
        docbow_project.docbow.views.inbox_by_document,
        name='inbox-by-document-message',
    ),
    path(
        'inbox/<int:mailbox_id>/',
        docbow_project.docbow.views.message,
        name='inbox-message',
        kwargs={'outbox': False},
    ),
    path(
        'inbox/<int:mailbox_id>/delete/',
        docbow_project.docbow.views.delete,
        name='inbox-message-delete',
    ),
    path(
        'inbox/<int:doc_id>/restore/',
        docbow_project.docbow.views.restore,
        name='inbox-message-restore',
        kwargs={'outbox': False},
    ),
    re_path(
        r'^inbox/(?P<mailbox_id>\d+)/(?P<attached_file>\d+)/.*$',
        docbow_project.docbow.views.message_attached_file,
        name='inbox-message-attached-file',
    ),
    path(
        'inbox/<int:mailbox_id>/allfiles/',
        docbow_project.docbow.views.message_all_files,
        name='outbox-message-attached-file',
    ),
    path('inbox/csv', docbow_project.docbow.views.inbox_csv, name='inbox-csv'),
    path('inbox/ods', docbow_project.docbow.views.inbox_ods, name='inbox-ods'),
    # outbox
    path('outbox/', docbow_project.docbow.views.outbox_view, name='outbox'),
    path('outbox/trash/', docbow_project.docbow.views.outbox_trash_view, name='outbox-trash'),
    path(
        'outbox/<int:mailbox_id>/',
        docbow_project.docbow.views.message,
        name='outbox-message',
        kwargs={'outbox': True},
    ),
    path(
        'outbox/<int:mailbox_id>/delete/',
        docbow_project.docbow.views.delete,
        name='outbox-message-delete',
        kwargs={'outbox': True},
    ),
    path(
        'outbox/<int:doc_id>/restore/',
        docbow_project.docbow.views.restore,
        name='outbox-message-restore',
        kwargs={'outbox': True},
    ),
    re_path(
        r'^outbox/(?P<mailbox_id>\d+)/(?P<attached_file>\d+)/.*$',
        docbow_project.docbow.views.message_attached_file,
        name='outbox-message-attached-file',
        kwargs={'outbox': True},
    ),
    path(
        'outbox/<int:mailbox_id>/allfiles/',
        docbow_project.docbow.views.message_all_files,
        name='outbox-message-attached-file',
        kwargs={'outbox': True},
    ),
    path('outbox/csv', docbow_project.docbow.views.outbox_csv, name='outbox-csv'),
    path('outbox/ods', docbow_project.docbow.views.outbox_ods, name='outbox-ods'),
    path('send_file/', docbow_project.docbow.views.send_file_selector, name='send-file-selector'),
    path('send_file/<int:file_type_id>/', docbow_project.docbow.views.send_file, name='send-file'),
    path('help/', docbow_project.docbow.views.help, name='help'),
    re_path(r'^help/(?P<pagename>[a-zA-Z0-9-/\.]+)$', docbow_project.docbow.views.help, name='help'),
    path('contact/', docbow_project.docbow.views.contact, name='contact'),
    path('logout/', docbow_project.docbow.views.logout, name='logout'),
    path('delegate/', docbow_project.docbow.views.delegate, name='delegate'),
    path('upload/', include('docbow_project.docbow.upload_urls')),
    re_path(r'^su/(?P<username>.*)/$', docbow_project.docbow.views.su, {'redirect_url': '/'}),
    re_path(r'^mailing-lists/', docbow_project.docbow.views.mailing_lists, name='mailing-lists'),
    re_path(r'^search-inbox/', docbow_project.docbow.views.search_inbox, name='search-inbox'),
    re_path(r'^search-outbox/', docbow_project.docbow.views.search_outbox, name='search-outbox'),
]

for custom in ('docbow_project.pfwb', 'docbow_project.pw'):
    if custom in settings.INSTALLED_APPS:
        urlpatterns += [
            path('', include('%s.urls' % custom)),
        ]


if 'mellon' in settings.INSTALLED_APPS:
    urlpatterns.append(path('accounts/mellon/', include('mellon.urls')))
