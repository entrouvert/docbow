from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('docbow', '0001_initial'),
    ]

    operations = [
        migrations.DeleteModel(
            name='DeletedMailbox',
        ),
    ]
