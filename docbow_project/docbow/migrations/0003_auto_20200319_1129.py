from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('docbow', '0002_auto_20190711_1812'),
    ]

    operations = [
        migrations.AlterField(
            model_name='docbowprofile',
            name='personal_email',
            field=models.EmailField(
                help_text='if you provide a personal email address, notifications of new documents will also be sent to this address.',
                max_length=254,
                verbose_name='personal email address',
                blank=True,
            ),
        ),
    ]
