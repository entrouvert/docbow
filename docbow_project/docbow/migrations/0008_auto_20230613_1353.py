# Generated by Django 3.2.16 on 2023-06-13 11:53

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('docbow', '0007_filetype_extra_senders'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mailinglist',
            name='name',
            field=models.CharField(max_length=1000, verbose_name='Name'),
        ),
    ]
