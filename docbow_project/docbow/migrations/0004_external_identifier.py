from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('docbow', '0003_auto_20200319_1129'),
    ]

    operations = [
        migrations.AddField(
            model_name='docbowprofile',
            name='external_id',
            field=models.CharField(max_length=256, verbose_name='External identifer', blank=True),
        ),
    ]
