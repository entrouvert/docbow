import django.core.validators
import django.utils.timezone
import picklefield.fields
from django.conf import settings
from django.db import migrations, models

import docbow_project.docbow.models


class Migration(migrations.Migration):
    dependencies = [
        ('auth', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AttachedFile',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('name', models.CharField(max_length=300, verbose_name='Name')),
                (
                    'content',
                    models.FileField(
                        upload_to=docbow_project.docbow.models.generate_filename,
                        max_length=300,
                        verbose_name='File',
                    ),
                ),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AutomaticForwarding',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
            ],
            options={
                'verbose_name': 'Automatic forwarding rule',
                'verbose_name_plural': 'Automatic forwarding rules',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Content',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('description', models.CharField(unique=True, max_length=128)),
            ],
            options={
                'ordering': ['description'],
                'verbose_name': 'Content',
                'verbose_name_plural': 'Contents',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Delegation',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                (
                    'by',
                    models.ForeignKey(
                        related_name='delegations_to',
                        verbose_name='From',
                        to=settings.AUTH_USER_MODEL,
                        on_delete=models.CASCADE,
                    ),
                ),
                (
                    'to',
                    models.ForeignKey(
                        related_name='delegations_by',
                        verbose_name='To',
                        to=settings.AUTH_USER_MODEL,
                        on_delete=models.CASCADE,
                    ),
                ),
            ],
            options={
                'ordering': ['by'],
                'db_table': 'auth_delegation',
                'verbose_name': 'Account delegation',
                'verbose_name_plural': 'Account delegations',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DeletedDocument',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
            ],
            options={
                'ordering': ('-document',),
                'verbose_name': 'deleted document',
                'verbose_name_plural': 'deleted documents',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DeletedMailbox',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('delegate', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DocbowProfile',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('is_guest', models.BooleanField(default=False, verbose_name='Guest user')),
                (
                    'mobile_phone',
                    models.CharField(
                        blank=True,
                        max_length=32,
                        verbose_name='Mobile phone',
                        validators=[django.core.validators.RegexValidator('^\\+\\d+$')],
                    ),
                ),
                (
                    'personal_email',
                    models.EmailField(
                        help_text='if you provide a personal email address, notifications of new documents will also be sent to this address.',
                        max_length=75,
                        verbose_name='personal email address',
                        blank=True,
                    ),
                ),
                (
                    'accept_notifications',
                    models.BooleanField(
                        default=True,
                        help_text='If unchecked you will not received notifications anymore, by email or SMS.',
                        verbose_name='Accept to be notified',
                    ),
                ),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Document',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('real_sender', models.CharField(max_length=64, verbose_name='Real sender', blank=True)),
                (
                    'date',
                    models.DateTimeField(default=django.utils.timezone.now, verbose_name="Date d'envoi"),
                ),
                ('comment', models.TextField(verbose_name='Comments', blank=True)),
                ('_timestamp', models.TextField(blank=True)),
                (
                    'private',
                    models.BooleanField(
                        default=False, help_text='delegates cannot see this document', verbose_name='Private'
                    ),
                ),
            ],
            options={
                'base_manager_name': 'objects',
                'ordering': ['-date'],
                'verbose_name': 'Document',
                'verbose_name_plural': 'Documents',
                'permissions': (('FORWARD_DOCUMENT', 'Can forward documents'),),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DocumentForwarded',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('automatic', models.BooleanField(default=False)),
                (
                    'from_document',
                    models.ForeignKey(
                        related_name='document_forwarded_to', to='docbow.Document', on_delete=models.CASCADE
                    ),
                ),
                (
                    'to_document',
                    models.ForeignKey(
                        related_name='document_forwarded_from', to='docbow.Document', on_delete=models.CASCADE
                    ),
                ),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FileType',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('name', models.CharField(unique=True, max_length=128)),
                ('is_active', models.BooleanField(default=True, verbose_name='is active')),
            ],
            options={
                'ordering': ['name'],
                'verbose_name': 'File type',
                'verbose_name_plural': 'File types',
            },
            bases=(docbow_project.docbow.models.NameNaturalKey, models.Model),
        ),
        migrations.CreateModel(
            name='FileTypeAttachedFileKind',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('name', models.CharField(max_length=128, verbose_name='name')),
                (
                    'mime_types',
                    models.TextField(
                        help_text='mime types separated by spaces, wildcards are allowed',
                        verbose_name='mime types',
                        blank=True,
                    ),
                ),
                (
                    'cardinality',
                    models.PositiveSmallIntegerField(
                        default=0,
                        help_text='zero is a special value setting no limitation',
                        verbose_name='cardinality',
                    ),
                ),
                (
                    'minimum',
                    models.PositiveSmallIntegerField(default=0, verbose_name='minimum number of files'),
                ),
                ('position', models.PositiveSmallIntegerField(verbose_name='position')),
                (
                    'file_type',
                    models.ForeignKey(
                        verbose_name='document type', to='docbow.FileType', on_delete=models.CASCADE
                    ),
                ),
            ],
            options={
                'ordering': ('file_type', 'position', 'name'),
                'verbose_name': 'file type attached file kind',
                'verbose_name_plural': 'file type attached file kinds',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Mailbox',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('outbox', models.BooleanField(default=False, db_index=True, verbose_name='Outbox message')),
                ('date', models.DateTimeField(auto_now_add=True)),
                (
                    'document',
                    models.ForeignKey(
                        related_name='mailboxes',
                        verbose_name='Document',
                        to='docbow.Document',
                        on_delete=models.CASCADE,
                    ),
                ),
                (
                    'owner',
                    models.ForeignKey(
                        related_name='documents',
                        verbose_name='Mailbox owner',
                        to=settings.AUTH_USER_MODEL,
                        on_delete=models.CASCADE,
                    ),
                ),
            ],
            options={
                'ordering': ['-date'],
                'verbose_name': 'Mailbox',
                'verbose_name_plural': 'Mailboxes',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MailingList',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('name', models.CharField(max_length=400, verbose_name='Name')),
                ('is_active', models.BooleanField(default=True, verbose_name='is active')),
                (
                    'mailing_list_members',
                    models.ManyToManyField(
                        related_name='members_lists',
                        verbose_name='Mailing lists members',
                        to='docbow.MailingList',
                        blank=True,
                    ),
                ),
                (
                    'members',
                    models.ManyToManyField(
                        related_name='mailing_lists',
                        verbose_name='Members',
                        to=settings.AUTH_USER_MODEL,
                        blank=True,
                    ),
                ),
            ],
            options={
                'ordering': ['name'],
                'verbose_name': 'Mailing list',
                'verbose_name_plural': 'Mailing lists',
            },
            bases=(docbow_project.docbow.models.NameNaturalKey, models.Model),
        ),
        migrations.CreateModel(
            name='Notification',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('create_dt', models.DateTimeField(auto_now_add=True)),
                ('kind', models.CharField(default='new-document', max_length=32)),
                ('done', models.BooleanField(default=False)),
                ('failure', models.TextField(null=True, blank=True)),
                ('ctx', picklefield.fields.PickledObjectField(null=True, editable=False, blank=True)),
                (
                    'document',
                    models.ForeignKey(blank=True, to='docbow.Document', null=True, on_delete=models.CASCADE),
                ),
                (
                    'user',
                    models.ForeignKey(
                        blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE
                    ),
                ),
            ],
            options={
                'ordering': ('-id',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='NotificationPreference',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('kind', models.CharField(max_length=8, verbose_name='kind')),
                ('value', models.BooleanField(default=True, verbose_name='value')),
                (
                    'filetype',
                    models.ForeignKey(
                        verbose_name='file type', to='docbow.FileType', on_delete=models.CASCADE
                    ),
                ),
                (
                    'user',
                    models.ForeignKey(
                        verbose_name='user', to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE
                    ),
                ),
            ],
            options={
                'ordering': ('user__last_name', 'user__first_name', 'kind'),
                'verbose_name': 'notification preference',
                'verbose_name_plural': 'notification preferences',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SeenDocument',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('document', models.ForeignKey(to='docbow.Document', on_delete=models.CASCADE)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)),
            ],
            options={
                'ordering': ('-document',),
                'verbose_name': 'seen document',
                'verbose_name_plural': 'seen documents',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SendingLimitation',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                (
                    'filetypes',
                    models.ManyToManyField(
                        related_name='filetype_limitation',
                        verbose_name='Limitation des types de fichier',
                        to='docbow.FileType',
                        blank=True,
                    ),
                ),
                (
                    'lists',
                    models.ManyToManyField(
                        related_name='lists_limitation',
                        verbose_name='Limitation des destinataires',
                        to='docbow.MailingList',
                    ),
                ),
                (
                    'mailing_list',
                    models.OneToOneField(
                        verbose_name='Mailing list', to='docbow.MailingList', on_delete=models.CASCADE
                    ),
                ),
            ],
            options={
                'verbose_name': 'Limitation par liste de destinataires',
                'verbose_name_plural': 'Limitation par liste de destinataires',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='filetypeattachedfilekind',
            unique_together={('name', 'file_type')},
        ),
        migrations.AddField(
            model_name='document',
            name='filetype',
            field=models.ForeignKey(
                verbose_name='Document type', to='docbow.FileType', on_delete=models.CASCADE
            ),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='document',
            name='reply_to',
            field=models.ForeignKey(
                related_name='replies',
                verbose_name='Reply to',
                blank=True,
                to='docbow.Document',
                null=True,
                on_delete=models.CASCADE,
            ),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='document',
            name='sender',
            field=models.ForeignKey(
                related_name='documents_sent',
                verbose_name='Sender',
                to=settings.AUTH_USER_MODEL,
                on_delete=models.CASCADE,
            ),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='document',
            name='to_list',
            field=models.ManyToManyField(
                to='docbow.MailingList', verbose_name='Groups to send to', blank=True
            ),
        ),
        migrations.AddField(
            model_name='document',
            name='to_user',
            field=models.ManyToManyField(
                related_name='directly_received_documents',
                verbose_name='Users to send to',
                to=settings.AUTH_USER_MODEL,
                blank=True,
            ),
        ),
        migrations.AddField(
            model_name='deletedmailbox',
            name='mailbox',
            field=models.ForeignKey(to='docbow.Mailbox', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='deleteddocument',
            name='document',
            field=models.ForeignKey(to='docbow.Document', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='deleteddocument',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='delegation',
            unique_together={('by', 'to')},
        ),
        migrations.AddField(
            model_name='automaticforwarding',
            name='filetypes',
            field=models.ManyToManyField(
                related_name='forwarding_rules', verbose_name='filetype', to='docbow.FileType'
            ),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='automaticforwarding',
            name='forward_to_list',
            field=models.ManyToManyField(
                related_name='as_recipient_forwarding_rules',
                verbose_name='Groups to forward to',
                to='docbow.MailingList',
                blank=True,
            ),
        ),
        migrations.AddField(
            model_name='automaticforwarding',
            name='forward_to_user',
            field=models.ManyToManyField(
                related_name='as_recipient_forwarding_rules',
                verbose_name='Users to forward to',
                to=settings.AUTH_USER_MODEL,
                blank=True,
            ),
        ),
        migrations.AddField(
            model_name='automaticforwarding',
            name='originaly_to_user',
            field=models.ManyToManyField(
                related_name='as_original_recipient_forwarding_rules',
                to=settings.AUTH_USER_MODEL,
                blank=True,
                help_text='At least one recipient must match for the rule to apply.',
                verbose_name='Original recipients',
            ),
        ),
        migrations.AddField(
            model_name='attachedfile',
            name='document',
            field=models.ForeignKey(
                related_name='attached_files',
                verbose_name='Attached to',
                to='docbow.Document',
                on_delete=models.CASCADE,
            ),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='attachedfile',
            name='kind',
            field=models.ForeignKey(
                verbose_name='attached file kind',
                blank=True,
                to='docbow.FileTypeAttachedFileKind',
                null=True,
                on_delete=models.CASCADE,
            ),
            preserve_default=True,
        ),
        migrations.CreateModel(
            name='DocbowGroup',
            fields=[],
            options={
                'verbose_name': 'Docbow admin group',
                'proxy': True,
            },
            bases=('auth.group',),
        ),
        migrations.CreateModel(
            name='DocbowUser',
            fields=[],
            options={
                'verbose_name': 'Docbow admin user',
                'proxy': True,
            },
            bases=('auth.user',),
        ),
        migrations.CreateModel(
            name='Inbox',
            fields=[],
            options={
                'verbose_name': 'Inbox',
                'proxy': True,
                'verbose_name_plural': 'Inboxes',
            },
            bases=('docbow.mailbox',),
        ),
        migrations.CreateModel(
            name='Outbox',
            fields=[],
            options={
                'verbose_name': 'Outbox',
                'proxy': True,
                'verbose_name_plural': 'Outboxes',
            },
            bases=('docbow.mailbox',),
        ),
    ]
