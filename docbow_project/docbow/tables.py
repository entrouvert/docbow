import django_tables2 as tables
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _

from docbow_project.docbow import models


class MailboxTable(tables.Table):
    class Meta:
        model = models.Document
        attrs = {'class': 'paleblue'}


# FIXEM: translation in inline templates are ignored
_('self')


class OutboxCsvTable(tables.Table):
    official_sender = tables.Column(accessor='sender.get_full_name', verbose_name=_('official_sender_header'))
    recipients = tables.Column(accessor='recipients', verbose_name=_('recipients_header'), orderable=False)
    real_sender = tables.TemplateColumn(
        '{% load i18n %}{% if record.real_sender %}{{ record.real_sender }}{% else %}{% trans "self" %}{% endif %}',
        verbose_name=_('real_sender_header'),
    )
    filetype = tables.Column(accessor='filetype', verbose_name=_('type_header'))
    filenames = tables.Column(accessor='filenames', verbose_name=_('filename_header'), orderable=False)
    date = tables.Column(accessor='date', verbose_name=_('date_header'))

    class Meta:
        model = models.Document
        fields = ('official_sender',)
        sequence = ('recipients', 'official_sender', 'real_sender', 'filetype', 'filenames', '...')
        attrs = {'class': 'paleblue mailbox-table'}
        empty_text = _('No message')


SELECT_ALL = mark_safe('<input type="checkbox" name="select-all" class="js-select-all"/>')


class OutboxBaseTable(tables.Table):
    official_sender = tables.Column(
        accessor='sender.get_full_name',
        order_by=('sender__last_name', 'sender__first_name', 'sender__username'),
        verbose_name=_('official_sender_header'),
    )
    extra_senders = tables.TemplateColumn(
        '{{ record.extra_senders_display }}',
        verbose_name=_('Additional senders'),
    )
    recipients = tables.Column(accessor='recipients', verbose_name=_('recipients_header'), orderable=False)
    real_sender = tables.TemplateColumn(
        '{% load i18n %}{% if record.real_sender %}{{ record.real_sender }}{% else %}{% trans "self" %}{% endif %}',
        order_by=('real_sender',),
        verbose_name=_('real_sender_header'),
    )
    filetype = tables.Column(accessor='filetype', verbose_name=_('type_header'))
    filenames = tables.Column(accessor='filenames', verbose_name=_('filename_header'), orderable=False)
    date = tables.TemplateColumn(
        '{% load humantime %}{{ record.date|humantime }}', verbose_name=_('date_header')
    )

    class Meta:
        model = models.Document
        fields = ('official_sender',)
        sequence = ('recipients', 'official_sender', 'real_sender', 'filetype', 'filenames', '...')
        attrs = {'class': 'paleblue mailbox-table refresh', 'id': 'outbox-table'}
        empty_text = _('No message')


class OutboxTrashTable(OutboxBaseTable):
    restore = tables.TemplateColumn(
        template_name='docbow/outbox_restore_column.html', orderable=False, verbose_name=_('Restore')
    )

    class Meta:
        model = models.Document
        fields = ('official_sender',)
        sequence = (
            'recipients',
            'official_sender',
            'extra_senders',
            'real_sender',
            'filetype',
            'filenames',
            '...',
        )
        attrs = {'class': 'paleblue mailbox-table refresh', 'id': 'outbox-table'}
        empty_text = _('No message')


class OutboxTable(OutboxBaseTable):
    select = tables.TemplateColumn(
        '<input type="checkbox", name="select" class="js-select" value="{{ record.id }}" class="js-select"/>',
        orderable=False,
        verbose_name=SELECT_ALL,
    )
    delete = tables.TemplateColumn(
        template_name='docbow/outbox_delete_column.html', orderable=False, verbose_name=' '
    )

    class Meta:
        model = models.Document
        fields = ('official_sender',)
        sequence = (
            'select',
            'recipients',
            'official_sender',
            'extra_senders',
            'real_sender',
            'filetype',
            'filenames',
            '...',
        )
        attrs = {'class': 'paleblue mailbox-table refresh', 'id': 'outbox-table'}
        empty_text = _('No message')


class InboxCsvTable(tables.Table):
    filetype = tables.Column(accessor='filetype', verbose_name=_('type_header'))
    filenames = tables.Column(accessor='filenames', verbose_name=_('filename_header'), orderable=False)
    sender = tables.Column(accessor='sender', verbose_name=_('sender_header'))
    date = tables.Column(accessor='date', verbose_name=_('date_header'))

    class Meta:
        model = models.Document
        attrs = {'class': 'paleblue mailbox-table'}
        empty_text = _('No message')


class InboxBaseTable(tables.Table):
    seen = tables.BooleanColumn(accessor='seen', yesno=' ,✔', verbose_name=' ', orderable=False)
    filetype = tables.Column(accessor='filetype', verbose_name=_('type_header'))
    filenames = tables.Column(accessor='filenames', verbose_name=_('filename_header'), orderable=False)
    recipients = tables.TemplateColumn(
        '''{% load docbow %}{% for recipient_user in record.delivered_to|intersect:related_users %}
      <span>{{ recipient_user|username }}</span>{% if not forloop.last %},{% endif %}
{% endfor %}''',
        orderable=False,
        verbose_name=_('recipients_header'),
    )
    sender = tables.TemplateColumn(
        '{{ record.sender_display }}',
        order_by=('sender__last_name', 'sender__first_name', 'sender__username'),
        verbose_name=_('sender_header'),
    )
    extra_senders = tables.TemplateColumn(
        '{{ record.extra_senders_display }}',
        verbose_name=_('Additional senders'),
    )
    date = tables.TemplateColumn(
        '{% load humantime %}{{ record.date|humantime }}', verbose_name=_('date_header')
    )

    class Meta:
        model = models.Document
        fields = (
            'seen',
            'filetype',
        )
        attrs = {'class': 'paleblue mailbox-table refresh', 'id': 'outbox-table'}
        empty_text = _('No message')


class InboxTrashTable(InboxBaseTable):
    restore = tables.TemplateColumn(
        template_name='docbow/inbox_restore_column.html', orderable=False, verbose_name=_('Restore')
    )

    class Meta:
        model = models.Document
        fields = (
            'select',
            'seen',
            'filetype',
        )
        attrs = {'class': 'paleblue mailbox-table refresh', 'id': 'inbox-table'}
        empty_text = _('No message')


class InboxTable(InboxBaseTable):
    select = tables.TemplateColumn(
        '<input type="checkbox" class="js-select" name="select" value="{{ record.id }}"/>',
        verbose_name=SELECT_ALL,
        orderable=False,
    )
    delete = tables.TemplateColumn(
        template_name='docbow/inbox_delete_column.html', orderable=False, verbose_name=' '
    )

    class Meta:
        model = models.Document
        fields = (
            'select',
            'seen',
            'filetype',
        )
        attrs = {'class': 'paleblue mailbox-table refresh', 'id': 'outbox-table'}
        empty_text = _('No message')
