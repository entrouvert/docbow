import sys


class AppSettings:
    __DEFAULTS = {
        'PERSONAL_EMAIL': True,
        'MOBILE_PHONE': True,
        'GROUP_LISTING': True,
        'PRIVATE_DOCUMENTS': False,
        'EDIT_EMAIL': False,
        'DELEGATE_TO_EXISTING_USER': True,
        'DEFAULT_ACCEPT_NOTIFICATIONS_FOR_GUEST': True,
    }

    def __init__(self, prefix):
        self.__prefix = prefix

    @property
    def settings(self):
        if not hasattr(self, '_settings'):
            from django.conf import settings

            self._settings = settings
        return self._settings

    @property
    def DOCBOW_MAILBOX_PER_PAGE(self):
        return getattr(self.settings, 'DOCBOW_MAILBOX_PER_PAGE', 20)

    @property
    def DOCBOW_MENU(self):
        from django.utils.translation import gettext_noop

        return getattr(
            self.settings,
            'DOCBOW_MENU',
            [
                ('send-file-selector', gettext_noop('send-file_menu')),
                ('inbox', gettext_noop('inbox_menu')),
                ('outbox', gettext_noop('outbox_menu')),
                ('docbow_admin:index', gettext_noop('admin_menu')),
                ('profile', gettext_noop('profile_menu')),
                ('mailing-lists', gettext_noop('mailing-lists')),
                ('help', gettext_noop('help_menu')),
                ('contact', gettext_noop('contact_menu')),
            ],
        )

    @property
    def BASE_URL(self):
        return getattr(self.settings, 'DOCBOW_BASE_URL', 'http://localhost:8000')

    @property
    def TRUNCATE_FILENAME(self):
        return getattr(self.settings, 'DOCBOW_TRUNCATE_FILENAME', 80)

    @property
    def MAX_FILE_SIZE(self):
        return getattr(self.settings, 'DOCBOW_MAX_FILE_SIZE', 10 * 1024 * 1024)

    @property
    def MIME_BUFFER_SIZE(self):
        return getattr(self.settings, 'DOCBOW_MIME_BUFFER_SIZE', 300000)

    def __getattr__(self, name):
        from django.conf import settings

        if name not in self.__DEFAULTS:
            raise AttributeError
        return getattr(settings, self.__prefix + name, self.__DEFAULTS[name])


app_settings = AppSettings(prefix='DOCBOW_')
app_settings.__name__ = __name__
sys.modules[__name__] = app_settings
