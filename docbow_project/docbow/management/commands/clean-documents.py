from django.core.management.base import BaseCommand

from ... import models


class Command(BaseCommand):
    '''Undelete all documents'''

    help = 'Undelete all documents'

    def handle(self, *args, **options):
        count = models.Document.objects.count()
        models.Document.objects.all().delete()
        print('Definitively deleted %d documents.' % count)
