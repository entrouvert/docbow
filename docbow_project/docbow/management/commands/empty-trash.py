from datetime import timedelta

from django.conf import settings
from django.core.management.base import BaseCommand
from django.db import transaction
from django.utils.timezone import now

from docbow_project.docbow.models import DeletedDocument


class Command(BaseCommand):
    help = 'Empty trash'

    @transaction.atomic
    def handle(self, *args, **kwargs):
        target_date = now() - timedelta(days=settings.TRASH_DURATION)
        for deleted_doc in DeletedDocument.objects.filter(soft_delete=True).filter(
            soft_delete_date__lte=target_date
        ):
            deleted_doc.soft_delete = False
            deleted_doc.save()
