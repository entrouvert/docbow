from django.core.management.base import BaseCommand
from django.db import transaction

from ... import notification


class Command(BaseCommand):
    args = '<directory>'
    help = 'Send notifications'

    @transaction.atomic
    def handle(self, *args, **options):
        notification.process_notifications()
