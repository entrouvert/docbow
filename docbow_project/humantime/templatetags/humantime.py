from django import template
from django.template.defaultfilters import date
from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.utils.timezone import localtime

from .. import utils

register = template.Library()


@register.filter
def humandate(dt):
    full_dt = date(dt, 'SHORT_DATE_FORMAT')
    s = f'<span title="{escape(full_dt)}">{escape(utils.datetime2human(dt))}</span>'
    return mark_safe(s)


@register.filter
def humantime(dt):
    dt = localtime(dt)
    full_dt = date(dt, 'SHORT_DATETIME_FORMAT')
    s = '<span title="{}">{}</span>'.format(
        escape(full_dt), escape(utils.datetime2human(dt, include_time=True))
    )
    return mark_safe(s)
