import django.contrib.staticfiles.views
import django.views.defaults
from django.conf import settings
from django.contrib import admin
from django.urls import include, path, re_path

import docbow_project.docbow.views

admin.autodiscover()

from .docbow.admin import site as docbow_admin_site  # noqa: E402

urlpatterns = [
    path('', include('docbow_project.docbow.urls')),
    re_path(r'^robots.txt', docbow_project.docbow.views.robots),
    path('accounts/', include('docbow_project.docbow.auth_urls')),
    re_path(r'^superadmin/', admin.site.urls, name='admin'),
    re_path(r'^admin/', docbow_admin_site.urls, name='docbow_admin'),
]

if settings.DEBUG:
    urlpatterns += [
        re_path(r'^static/(?P<path>.*)$', django.contrib.staticfiles.views.serve),
    ]

    if 'debug_toolbar' in settings.INSTALLED_APPS:
        import debug_toolbar  # pylint: disable=import-error

        urlpatterns = [
            path('__debug__/', include(debug_toolbar.urls)),
        ] + urlpatterns
