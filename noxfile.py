from pathlib import Path

import nox

TESTENVS = {
    'pw': {
        'packages': [],
        'env': {'PARLEMENT': 'pw'},
        'testfiles': ['tests/main/', 'tests/pw/'],
    },
    'pfwb': {
        'packages': ['SQLAlchemy-Utils'],
        'env': {'PARLEMENT': 'pfwb'},
        'testfiles': ['tests/main/', 'tests/pfwb/'],
    },
    'mellon': {
        'packages': ['django-mellon'],
        'env': {'DOCBOW_SETTINGS_FILE': 'tests/sso/settings.py'},
        'testfiles': ['tests/sso/'],
    },
    'pfwbmellon': {
        'packages': ['SQLAlchemy-Utils', 'django-mellon'],
        'env': {'DOCBOW_SETTINGS_FILE': 'tests/pfwbmellon/settings.py'},
        'testfiles': ['tests/pfwbmellon/'],
    },
}


def run_hook(name, *args, **kwargs):
    for file in [Path(__name__).parent / '.nox-hooks.py', Path('~/.config/nox/eo-hooks.py').expanduser()]:
        if not file.exists():
            continue

        globals_ = {}
        exec(file.read_text(), globals_)
        hook = globals_.get(name, None)
        if hook:
            hook(*args, **kwargs)


def get_lasso3(session):
    src_dir = Path('/usr/lib/python3/dist-packages/')
    venv_dir = Path(session.virtualenv.location)
    for dst_dir in venv_dir.glob('lib/**/site-packages'):
        files_to_link = [src_dir / 'lasso.py'] + list(src_dir.glob('_lasso.cpython-*.so'))

        for src_file in files_to_link:
            dst_file = dst_dir / src_file.name
            if dst_file.exists():
                dst_file.unlink()
            session.log('%s => %s', dst_file, src_file)
            dst_file.symlink_to(src_file)


def setup_venv(session, *packages, django_version='', env=''):
    packages = [
        f'django{django_version}',
        'psycopg2-binary',
        'mock',
        'pytest',
        'pytest-cov',
        'pytest-django',
        'django-webtest',
        'pyquery',
        'git+https://git.entrouvert.org/entrouvert/django-journal.git',
        *packages,
    ]
    run_hook('setup_venv', session, packages)
    session.install('-e', '.', *packages, silent=False)
    get_lasso3(session)


def hookable_run(session, *args, **kwargs):
    args = list(args)
    run_hook('run', session, args, kwargs)
    session.run(*args, **kwargs)


@nox.session()
@nox.parametrize('django', ['>=3.2.12,<3.3', '>=4.2,<4.3'])
@nox.parametrize('testenv', ['pw', 'pfwb', 'mellon', 'pfwbmellon'])
def tests(session, django, testenv):
    setup_venv(
        session,
        *TESTENVS[testenv]['packages'],
        django_version=django,
    )

    session.run('python3', 'setup.py', 'compile_translations')

    args = ['py.test']
    if '--coverage' in session.posargs or not session.interactive:
        while '--coverage' in session.posargs:
            session.posargs.remove('--coverage')
        args += [
            '--cov-report',
            'xml',
            '--cov-report',
            'html',
            '--cov-context',
            'test',
            '--cov=docbow_project/',
            '--cov-config',
            '.coveragerc',
            '-v',
            f'--junitxml=junit-coverage.django-{django}.xml',
        ]

    args += session.posargs + TESTENVS[testenv]['testfiles']

    env = {
        'DJANGO_SETTINGS_MODULE': 'docbow_project.settings',
        'DOCBOW_SETTINGS_FILE': 'tests/settings.py',
        'DB_ENGINE': 'django.db.backends.postgresql_psycopg2',
    }
    env.update(TESTENVS[testenv]['env'])

    hookable_run(session, *args, env=env)


@nox.session
def codestyle(session):
    session.install('pre-commit')
    session.run('pre-commit', 'run', '--all-files', '--show-diff-on-failure')
