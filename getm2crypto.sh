#!/bin/sh

# Get venv site-packages path
DSTDIR=`python -c 'import sysconfig; print(sysconfig.get_path("purelib"))'`
NAME="M2Crypto"

# Get not venv site-packages path
# Remove first path (assuming that is the venv path)
NONPATH=`echo $PATH | sed 's/^[^:]*://'`
if ! /usr/bin/python -c 'import M2Crypto' >/dev/null 2>&1; then
	echo M2Crypto package is not installed
	exit 1
fi
SRCDIR=`/usr/bin/python -c 'import sysconfig; print(sysconfig.get_path("purelib"))'`

# Clean up
rm -f $DSTDIR/$NAME
rm -f $DSTDIR/$NAME*.egg-info

# Link
ln -sv $SRCDIR/$NAME $DSTDIR
ln -sv $SRCDIR/$NAME*.egg-info $DSTDIR

exit 0

