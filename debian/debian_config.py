# This file is sourced by "execfile" from docbow_project.settings

import glob
import os

PROJECT_NAME = 'docbow'

ETC_DIR = os.path.join('/etc', PROJECT_NAME)
ETC_SETTINGS_PY = os.path.join(ETC_DIR, 'settings.py')


if os.path.exists(ETC_SETTINGS_PY):
    exec(open(ETC_SETTINGS_PY).read())

for filename in sorted(glob.glob(os.path.join(ETC_DIR, 'settings.d', '*.py'))):
    exec(open(filename).read())
