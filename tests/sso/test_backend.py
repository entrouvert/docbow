import pytest
from django.contrib.auth.models import User

from docbow_project.docbow.auth_backend import DocbowMellonAuthBackend
from docbow_project.docbow.models import Delegation, DocbowProfile


@pytest.mark.django_db
def test_regular_user():
    user = User.objects.create(username='someuser')
    assert DocbowMellonAuthBackend().get_user(user.pk) == user


@pytest.mark.django_db
def test_delegate_user():
    delegate = User.objects.create(username='some-delegate_user')
    user = User.objects.create(username='someuser')
    DocbowProfile.objects.create(user=delegate, is_guest=True)
    Delegation.objects.get_or_create(by=user, to=delegate)

    res = DocbowMellonAuthBackend().get_user(delegate.pk)
    assert res == user
    assert res.delegate == delegate
