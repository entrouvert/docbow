import os

SECRET_KEY = 'coin'

INSTALLED_APPS += ('mellon',)  # noqa: F821

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'TEST': {
            'NAME': 'docbow-test-%s' % os.environ.get('BRANCH_NAME', '').replace('/', '-')[:63],
        },
    }
}
