from datetime import timedelta

from django.contrib.auth.models import User
from django.core import management
from django.utils.timezone import localtime, now
from utils import assert_can_see_doc, send_file

from docbow_project.docbow.models import DeletedDocument, Document


def test_forward_docs(app, users, settings, filetypes):
    sender = User.objects.get(username='user-1')
    recipient = User.objects.get(username='user-2')
    new_user = User.objects.get(username='user-3')

    send_file(app, settings, sender, recipient)
    send_file(app, settings, sender, recipient)

    doc_ids = [doc.pk for doc in Document.objects.filter(sender=sender)]
    assert len(doc_ids) == 2
    management.call_command('forward-docs', '%s' % recipient.pk, '%s' % new_user.pk)
    assert Document.objects.filter(sender=sender).count() == 4
    for doc in Document.objects.filter(sender=sender).exclude(pk__in=doc_ids):
        assert_can_see_doc(app, doc, new_user)


def test_forward_docs_date_filter(app, users, settings, filetypes):
    sender = User.objects.get(username='user-1')
    recipient = User.objects.get(username='user-2')
    new_user = User.objects.get(username='user-3')

    send_file(app, settings, sender, recipient)
    doc1 = Document.objects.get(sender=sender)
    doc1.date = localtime(now()) - timedelta(days=30)
    doc1.save()

    send_file(app, settings, sender, recipient)
    doc2 = Document.objects.filter(sender=sender).exclude(pk=doc1.pk).first()
    doc2.date = localtime(now()) + timedelta(days=30)
    doc2.save()

    send_file(app, settings, sender, recipient)
    doc3 = Document.objects.filter(sender=sender).exclude(pk__in=[doc1.pk, doc2.pk]).first()

    assert Document.objects.filter(sender=sender).count() == 3
    management.call_command(
        'forward-docs',
        '%s' % recipient.pk,
        '%s' % new_user.pk,
        startdate=(localtime(now()) - timedelta(days=20)),
        enddate=(localtime(now()) + timedelta(days=20)),
    )

    assert Document.objects.filter(sender=sender).count() == 4
    doc = Document.objects.filter(sender=sender).exclude(pk__in=[doc1.pk, doc2.pk, doc3.pk]).first()
    assert_can_see_doc(app, doc, new_user)


def test_forward_docs_exclude_already_received_docs(app, users, settings, filetypes):
    sender = User.objects.get(username='user-1')
    recipient = User.objects.get(username='user-2')
    new_user = User.objects.get(username='user-3')
    send_file(app, settings, sender, [recipient, new_user])
    assert Document.objects.filter(sender=sender).count() == 1

    management.call_command('forward-docs', '%s' % recipient.pk, '%s' % new_user.pk)
    # Nothing done
    assert Document.objects.filter(sender=sender).count() == 1


def test_empty_trash(app, users, settings, filetypes):
    settings.TRASH_DURATION = 1
    sender = User.objects.get(username='user-1')
    recipient = User.objects.get(username='user-2')
    send_file(app, settings, sender, recipient)
    doc = Document.objects.get()
    assert DeletedDocument.objects.count() == 0

    management.call_command('empty-trash')
    # Nothing done
    assert DeletedDocument.objects.count() == 0

    deleted_doc = DeletedDocument.objects.create(document=doc, user=recipient)
    assert deleted_doc.soft_delete is False
    assert deleted_doc.soft_delete_date is None
    management.call_command('empty-trash')
    deleted_doc.refresh_from_db()
    assert deleted_doc.soft_delete is False
    assert deleted_doc.soft_delete_date is None

    deleted_doc.soft_delete = True
    now_date = now()
    deleted_doc.soft_delete_date = now_date
    deleted_doc.save()
    management.call_command('empty-trash')
    deleted_doc.refresh_from_db()
    assert deleted_doc.soft_delete is True
    assert deleted_doc.soft_delete_date == now_date

    days = settings.TRASH_DURATION + 1
    past_obj = now() - timedelta(days=days)
    deleted_doc.soft_delete_date = past_obj
    deleted_doc.save()
    management.call_command('empty-trash')
    deleted_doc.refresh_from_db()
    assert deleted_doc.soft_delete is False
    assert deleted_doc.soft_delete_date == past_obj
