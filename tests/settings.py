import os

SECRET_KEY = 'coin'

DOCBOW_PRIVATE_DOCUMENTS = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'TEST': {
            'NAME': 'docbow-test-%s' % os.environ.get('BRANCH_NAME', '').replace('/', '-')[:63],
        },
    }
}

PARLEMENT = os.environ['PARLEMENT']

if PARLEMENT == 'pfwb':
    INSTALLED_APPS += ('docbow_project.pfwb',)  # noqa: F821
    DOCBOW_EDIT_EMAIL = True
    TABELLIO_DBNAME = 'tabellio-test-%s' % os.environ.get('BRANCH_NAME', '').replace('/', '-')[:63]
    TABELLIO_DBHOST = ''
    TABELLIO_DBPORT = ''
    TABELLIO_DBUSER = ''
    TABELLIO_DBPASSWORD = ''

elif PARLEMENT == 'pw':
    INSTALLED_APPS += ('docbow_project.pw',)
    DOCBOW_PERSONAL_EMAIL = False
    DOCBOW_MOBILE_PHONE = False
    DOCBOW_GROUP_LISTING = False
    DOCBOW_DEFAULT_ACCEPT_NOTIFICATIONS_FOR_GUEST = False
