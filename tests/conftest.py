import os
import sys

import pytest
from django.contrib.auth.models import User

from docbow_project.docbow.models import DocbowProfile, FileType

sys.path.append(os.path.join(os.path.dirname(__file__), 'helpers'))


@pytest.fixture
def filetypes(db):
    result = []
    for i in range(10):
        result.append(FileType.objects.create(name='filetype-%s' % i))
    return result


@pytest.fixture
def users(db):
    result = []
    for i in range(10):
        result.append(User.objects.create(username='user-%s' % i, email='user-%s@example.com' % i))
        result[-1].set_password('password')
        result[-1].save()
        DocbowProfile.objects.create(user=result[-1], personal_email='personal-email-user-%s@example.com' % i)
    return result


@pytest.fixture
def admin(db):
    user = User.objects.create(username='admin', email='admin@localhost', is_staff=True, is_superuser=True)
    user.set_password('password')
    user.save()
    return user


@pytest.fixture
def app(django_app):
    def login(username='user-1', password='password'):
        login_page = django_app.get('/accounts/login/')
        login_form = login_page.forms[0]
        login_form['username'] = username
        login_form['password'] = password
        resp = login_form.submit()
        assert resp.status_int == 302
        return resp

    def logout():
        resp = django_app.get('/logout/')
        assert resp.status_int == 302
        return resp

    django_app.login = login
    django_app.logout = logout

    return django_app
