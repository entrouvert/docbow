import os

SECRET_KEY = 'coin'

INSTALLED_APPS += ('mellon', 'docbow_project.pfwb')  # noqa: F821

MELLON_ADAPTER = ('docbow_project.pfwb.mellon_adapter.PFWBMellonAdapter',)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'TEST': {
            'NAME': 'docbow-test-%s' % os.environ.get('BRANCH_NAME', '').replace('/', '-')[:63],
        },
    }
}
