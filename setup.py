#!env python

import os
import subprocess
import sys

from setuptools import Command, find_packages, setup
from setuptools.command.build import build as _build
from setuptools.command.install_lib import install_lib as _install_lib
from setuptools.command.sdist import sdist


class eo_sdist(sdist):
    def run(self):
        if os.path.exists('VERSION'):
            os.remove('VERSION')
        version = get_version()
        version_file = open('VERSION', 'w')
        version_file.write(version)
        version_file.close()
        sdist.run(self)
        if os.path.exists('VERSION'):
            os.remove('VERSION')


def get_version():
    """Use the VERSION, if absent generates a version with git describe, if not
    tag exists, take 0.0- and add the length of the commit log.
    """
    if os.path.exists('VERSION'):
        with open('VERSION') as v:
            return v.read()
    if os.path.exists('.git'):
        p = subprocess.Popen(
            ['git', 'describe', '--dirty=.dirty', '--match=v*'],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        result = p.communicate()[0]
        if p.returncode == 0:
            result = result.decode('ascii').strip()[1:]  # strip spaces/newlines and initial v
            if '-' in result:  # not a tagged version
                real_number, commit_count, commit_hash = result.split('-', 2)
                version = '%s.post%s+%s' % (real_number, commit_count, commit_hash)
            else:
                version = result.replace('.dirty', '+dirty')
            return version
        else:
            return '0.0.post%s' % len(subprocess.check_output(['git', 'rev-list', 'HEAD']).splitlines())
    return '0.0'


class compile_translations(Command):
    description = 'compile message catalogs to MO files via django compilemessages'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        try:
            from django.core.management import call_command

            for path, dirs, files in os.walk('docbow_project'):
                if 'locale' not in dirs:
                    continue
                curdir = os.getcwd()
                os.chdir(os.path.realpath(path))
                call_command('compilemessages')
                os.chdir(curdir)
        except ImportError:
            sys.stderr.write('!!! Please install Django >= 1.4 to build translations\n')


class build(_build):
    sub_commands = [('compile_translations', None)] + _build.sub_commands


class install_lib(_install_lib):
    def run(self):
        self.run_command('compile_translations')
        _install_lib.run(self)


setup(
    name='docbow',
    version=get_version(),
    license='AGPL 3.0',
    description='Document box for the Wallon  Parliament',
    url='https://dev.entrouvert.org/projects/docbow-pub/',
    author="Entr'ouvert",
    author_email='info@entrouvert.com',
    maintainer='Benjamin Dauvergne',
    maintainer_email='bdauvergne@entrouvert.com',
    include_package_data=True,
    scripts=('manage.py',),
    packages=find_packages(),
    install_requires=[
        'django>=3.2, <4.3',
        'typing',  # For M2Crypto.util
        'django_journal>=2.0.0',
        'django-picklefield',
        'django-tables2',
        'requests',
        'python-magic',
        'django-watson',
        'sqlalchemy<2',
        'beautifulsoup4',
    ],
    cmdclass={
        'build': build,
        'install_lib': install_lib,
        'compile_translations': compile_translations,
        'sdist': eo_sdist,
    },
)
