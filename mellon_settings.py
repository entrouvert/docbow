# Exemple settings to use for django-mellon SSO
# Except MELLON_ATTRIBUTE_MAPPING, everything is mandatory.
#
# AUTHENTIC URL and credentials are used
# to manage guest users.

INSTALLED_APPS += ('mellon',)  # noqa: F821

AUTHENTICATION_BACKENDS = (
    'docbow_project.docbow.auth_backend.DocbowMellonAuthBackend' 'django.contrib.auth.backends.ModelBackend'
)

LOGIN_URL = 'mellon_login'
LOGOUT_URL = 'mellon_logout'

MELLON_IDENTITY_PROVIDERS = [{'METADATA': '/path/to/idp.xml'}]
MELLON_PUBLIC_KEYS = ['/path/to/saml.crt']
MELLON_PRIVATE_KEY = '/path/to/saml.key'

AUTHENTIC_URL = 'https://authentic.url/'
AUTHENTIC_USER = 'login'
AUTHENTIC_PASSWORD = 'password'

# Authentic role uuid - created users will be assigned this role
AUTHENTIC_ROLE = 'some-role-uuid'

MELLON_ATTRIBUTE_MAPPING = {
    'email': '{attributes[email][0]}',
    'first_name': '{attributes[first_name][0]}',
    'last_name': '{attributes[last_name][0]}',
}

MELLON_SUPERUSER_MAPPING = {
    'is_superuser': 'true',
}
