Document box for the Wallon Parliement
======================================

This software is a web application allowing group of people to send files to
each other and to list of them. It distributes and timestamps the sent
documents. The timestamp is a cryptographic timestamp obtained using the
RFC3161 protocol.

The application is pre-configured to run with a postgresql database named
docbow. The user running the application need access to this database. On most
Unix system it means creating a postgresql user with the same name as the Unix
user it the database server runs on the same host.

Installation for developpers
----------------------------

 - First run:

   ./start.sh

 - and next times:

   ./run.sh

 - To update dependencies in the virtualenv environment rerun ./start.sh

Installation
------------

 - Create a virtualenv::

     virtualenv --no-site-packages env/

 - Activate it::

     . ./env/bin/activate 

 - Install dependencies (some dependencies require to compile C code, like
   python-ldap, for them you will some developement package installed, on
   Debian, it means gcc, python-dev, libssl-dev, libldap-dev, libsasl2-dev,
   libpq-dev)::
     ./getlasso.sh
     pip install -e .

 - Create user and database (you can replace $USER by www-data for a WSGI
   installation)::

     sudo -u postgres createuser $USER
     sudo -u postgres createdb -O $USER docbow

 - Fill in your local settings
   Rename locale_settings.py.example to local_settings.py, and fill it
   with appropriate database informations (standard Django settings.py file like)
   Also put DEBUG to True or set the SECRET_KEY

 - Create database schema and load initial data::

      docbow-ctl syncdb
      docbow-ctl loaddata content filetype groups

 - Compile UI strings translations::

     docbow-ctl makemessages --all
     docbow-ctl compilemessages

 - Load user list (you need to get a user list as a CSV files, see format later)::

     ( cd docbow_project; docbow-ctl load-users-csv users.csv )

Upgrading
---------

When you upgrade you must execute this from the root directory:

     ( cd docbow_project; docbow-ctl syncdb --migrate )

if you application run as another user (www-data for example if using WSGI)::

     ( cd docbow_project; sudo -u www-data docbow-ctl syncdb --migrate )

Installation on Debian with Apache2/mod_wsgi
--------------------------------------------

First install some development libraries::

     sudo apt-get install gcc libldap-dev libsasl2-dev python-dev libssl-dev libpq-dev swig gettext

Now you can follow the generic installation described before.

Collect all static content::

    (cd docbow_project; docbow-ctl collectstatic)

After that you must install apache2 and the mod_wsgi module. On Debian do::

     sudo apt-get install apache2 libapache2-mod-wsgi

You must configure the apache2 script to launch with an UTF-8 locale. On debian
you can add the following line to /etc/default/apache2 (supposing you have the
french locale with UTF-8 encoding compiled)::

     ENV="$ENV LANG=fr_FR.UTF-8 LC_ALL=fr_FR.UTF-8"

Then you must add a virtual host configuration which target the docbow wsgi
script. On debian you can put the following content in
/etc/apache2/sites-enabled/docbow.example.com, do not forget to replace
<docbow_src_dir> by the source directory where docbow is installed::

     <VirtualHost *:80>
     ServerName docbow.example.com
     
     Alias /static <docbow_src_dir>/static
     WSGIScriptAlias / <docbow_src_dir>/docbow.wsgi
     RedirectMatch 404 ^/favicon.ico$
     
     </VirtualHost>

Then you must activate it with the commands::

     a2ensite docbow.example.com
     /etc/init.d/apache2 reload

Installation on Debian using gunicorn and apache2
-------------------------------------------------

First install some development libraries::

     apt-get install gcc libldap-dev libsasl2-dev python-dev libssl-dev libpq-dev swig gettext

Now you can follow the generic installation described before.

Install gunicorn::

     ./env/bin/pip install gunicorn

Collect all static content::

    (cd docbow_project; docbow-ctl collectstatic)

After that you must install apache2 and the mod_wsgi module. On Debian do::

     sudo apt-get install apache2

Then you must add a virtual host configuration which target the gunicorn
application server. On debian you can put the following content in
/etc/apache2/sites-enabled/docbow.example.com, do not forget to replace
<docbow_src_dir> by the source directory where docbow is installed::

     <VirtualHost *:80>
     ServerName docbow.example.com

     DocumentRoot /var/www
     ProxyPass /static !
     ProxyPass / http://localhost:8000/
     ProxyPreserveHost on
     Alias /static <docbow_src_dir>/static
     <Proxy *>
     Order Deny,Allow
     Allow from all
     </Proxy>
     </VirtualHost>

Now run the gunicorn server (it must run as an user which has access to the
docbow postgresql database)::

     LANG=C.UTF-8 PYTHONPATH=. gunicorn_django -D ./docbow_project/

You must set a charset to use as file are created with name recevied from user
agents which can contain any unicode character. The locale "C.UTF-8" works in
this case.

Configuring main menu
---------------------

To configure the main menu you can setup the DOCBOW_MENU variable in your
`local_settings.py` file. The default setting is: ::

    DOCBOW_MENU = [
            ('send-file', gettext_noop('send-file_menu')),
            ('inbox', gettext_noop('inbox_menu')),
            ('outbox', gettext_noop('outbox_menu')),
            ('docbow_admin:index', gettext_noop('admin_menu')),
            ('profile', gettext_noop('profile_menu')),
            ('auth_password_change', gettext_noop('password_change_menu')),
            ('delegate', gettext_noop('delegate_menu')),
            ('mailing-lists', gettext_noop('mailing-lists')),
            ('help', gettext_noop('help_menu')),
            ('contact', gettext_noop('contact_menu')),
            ]

The first element of each pair must be a django view name of an URL, the second
one is potentially localized, for example to add a link to google, add this line::

            ('http://google.com/', u'Google'),

Settings
--------

All settings must be donne in the file ``/etc/docbow/local_settings.py``. Available settings are:

 * ``DOCBOW_ORGANIZATION``: an unicode string giving a description of the
   organization providing the platform. It's used as a signature in mail and
   sms notifications.
 * ``DOCBOW_BASE_URL``: the base URL of the application. It's used for building
   URL in notifications, emails or SMS.
 * ``DOCBOW_MENU``: description of the left column menu; see previous section
   for a description and the default value.
 * ``DOCBOW_MAILBOX_PER_PAGE``: the number of message to show on listing pages.
   Default is 20.
 * ``RAVEN_CONFIG_DSN``: the URL of the sentry project for gathering exceptions.
 * ``DOCBOW_MAX_FILE_SIZE``: the maximum file size for attached files, as
   bytes. Default is 10 Mo.
 * ``DOCBOW_TRUNCATE_FILENAME``: the maximum length for filenames. Default is
   80 unicode characters (codepoints).
 * ``DOCBOW_PERSONAL_EMAIL``: allow user to have a personal email,
   notifications will be sent to their institutional email and personal email.
 * ``DOCBOW_MOBILE_PHONE``: allow user to set a mobile phone number
   to receive notification by SMS.
 * ``GROUP_LISTING``: whether to show the link to the list of
   mailing lists in the left menu.
 * ``DOCBOW_PRIVATE_DOCUMENTS``: add a private checkbox to the sending form,
   when checked the document is only display to direct recipients and not to
   delegates,

Customizing templates
---------------------

You can override any template by copying it from
``docbow_project/docbow/templates/`` to ``/var/lib/docbow/templates/`` and then
modifying it. If you want to provide static ressources you can put them in
``/var/lib/docbow/extra_static/`` and update the static files cache with: ::

   ./docbow-ctl collectstatic

Sending a file from the command line
------------------------------------

The ``sendfile`` command is used to send a file from the command line. Basic
usage is::


  ./docbow-ctl sendfile --sender bdauvergne --to-user pcros --to-list liste1 \
      --description "New question" file1 file2

The sender and filetype parameters are mandatory. There must be at least one
recipient, list or user, and one file attached to the sending.

Listing users from the command line
-----------------------------------

The ``list-users`` command is used to list the user registerd in docbow. The
default output format is an ASCII table. You can ask for CSV output using the
``--csv`` parameter.

Example output::

|  Id |     Username | First name | Last name |               Email | Mobile phone |   Personal mail |       Lists |          Groups |
| 502 |        test3 |     Thomas |      Noël |       bob@gmail.com |              |                 | Liste test2 |                 |
| 503 | bdauvergne-1 |   benjamin | dauvergne |      test@gmail.com |              |                 |             |                 |
| 501 |        test2 |      Kévin |   Gaspard |     test2@gmail.com |              |                 | Liste test1 |                 |
| 504 |          503 |            |           |    toto@example.com |              |                 |             |                 |
| 444 |         test |     Jérôme | Schneider |  john.doe@gmail.com |              |                 | Liste test1 |                 |
|  98 |   bdauvergne |   Benjamin | Dauvergne | coin@entrouvert.com | +33630XXX893 | joe@example.com |             | Administrateurs |

The command supports a minimal query language, for example to get all users in
the group ``Administrator`` ::


  ./docbow-ctl list-users groups__name=Administrator

Listing lists from the command line
-----------------------------------

The ``list-lists`` command provide exactly the same service as ``list-users``
but for mailing lists.

Example output::

| Id |        Name |     Members | List Members |
| 54 | Liste test1 |             |   test,test2 |
| 55 | Liste test2 | Liste test1 |        test3 |
| 56 |  List test1 |             |              |

Adding users from the command line
----------------------------------

The ``add-user`` create a new user or modify an existing user. The username can
only be set at creation.::

  Usage: ./docbow-ctl add-user [options] <username>

  Create a new user or update an existing user

  List and groups can be referred by name or by id.
      

  Options:
    -v VERBOSITY, --verbosity=VERBOSITY
                          Verbosity level; 0=minimal output, 1=normal output,
                          2=verbose output, 3=very verbose output
    --settings=SETTINGS   The Python path to a settings module, e.g.
                          "myproject.settings.main". If this isn't provided, the
                          DJANGO_SETTINGS_MODULE environment variable will be
                          used.
    --pythonpath=PYTHONPATH
                          A directory to add to the Python path, e.g.
                          "/home/djangoprojects/myproject".
    --traceback           Print traceback on exception
    --first-name=FIRST_NAME
                          set first name
    --last-name=LAST_NAME
                          set last name
    --email=EMAIL         set email
    --mobile-phone=MOBILE_PHONE
                          set mobile phone used for SMS notifications
    --personal-email=PERSONAL_EMAIL
                          set personal email
    --add-list=ADD_LIST   add user to list
    --add-group=ADD_GROUP
                          add user to group
    --remove-list=REMOVE_LIST
                          remove user from list
    --remove-group=REMOVE_GROUP
                          remove user from group
    --version             show program's version number and exit
    -h, --help            show this help message and exit

Adding lists from the command line
----------------------------------

The ``add-list`` command allows to create new lists and to update their
members, lists or users::

  usage: docbow-ctl add-list [-h] [--version] [-v {0,1,2,3}]
                           [--settings SETTINGS] [--pythonpath PYTHONPATH]
                           [--traceback] [--no-color] [--add-list ADD_LIST]
                           [--remove-list REMOVE_LIST] [--add-user ADD_USER]
                           [--remove-user REMOVE_USER]
                           ml_name

  Create or update a list

  positional arguments:
    ml_name               Name of the mailing list

  optional arguments:
    -h, --help            show this help message and exit
    --version             show program's version number and exit
    -v {0,1,2,3}, --verbosity {0,1,2,3}
                          Verbosity level; 0=minimal output, 1=normal output,
                          2=verbose output, 3=very verbose output
    --settings SETTINGS   The Python path to a settings module, e.g.
                          "myproject.settings.main". If this isn't provided, the
                          DJANGO_SETTINGS_MODULE environment variable will be
                          used.
    --pythonpath PYTHONPATH
                          A directory to add to the Python path, e.g.
                          "/home/djangoprojects/myproject".
    --traceback           Raise on CommandError exceptions
    --no-color            Don't colorize the command output.
    --add-list ADD_LIST   add a list as a sublist
    --remove-list REMOVE_LIST
                          remove list as a sublist
    --add-user ADD_USER   add a user member
    --remove-user REMOVE_USER
                          remove a user member

